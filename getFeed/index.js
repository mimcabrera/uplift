"use strict"

let _ = require('lodash')
let config = require('config')
let AWS = require('aws-sdk')
let DOC = require('dynamodb-doc')
let DEFAULT_LIMIT = process.env.PAGE_FEED_LIMIT || 50

let AWS_ACCESS_KEY = process.env.NODE_ENV === 'production' ? null : process.env['AWS_ACCESS_KEY'] || config.get('AWS_ACCESS_KEY')
let AWS_SECRET_KEY = process.env.NODE_ENV === 'production' ? null : process.env['AWS_SECRET_KEY'] || config.get('AWS_SECRET_KEY')
let AWS_REGION = process.env.NODE_ENV === 'production' ? null : process.env['AWS_REGION'] || config.get('AWS_REGION')

exports.handler = (event, context, callback) => {

  if (!AWS_ACCESS_KEY || !AWS_SECRET_KEY || !AWS_REGION) {
    console.log('AWS credentials is missing. Are you running in production ? If not, please try to set your environment variables or define a config file (/config/default.json)')
  } else {
    AWS.config.update({
      accessKeyId: AWS_ACCESS_KEY,
      secretAccessKey: AWS_SECRET_KEY,
      region: AWS_REGION
    })
  }

  let docClient = new DOC.DynamoDB()

  let lastEvaluatedKey = _.get(event, 'params.querystring.lastEvaluatedKey')
  let limit = _.get(event, 'params.querystring.limit', DEFAULT_LIMIT)

  let filter = {
    TableName: 'Uplift_Feed',
    Limit: limit
  }

  if (lastEvaluatedKey) _.assign(filter, {
    ExclusiveStartKey: lastEvaluatedKey
  })

  docClient.scan(filter, callback)
}