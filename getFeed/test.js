"use strict"

let chai = require('chai')
let expect = require('chai').expect
let assertArrays = require('chai-arrays');
chai.use(assertArrays);

let LambdaTester = require('lambda-tester')

let lambdaToTest = require('./index.js')

describe('When system tries to run GetFeed', function() {
  it('should not have errored', function() {
    return LambdaTester(lambdaToTest.handler)
      .expectResult( function(res) {
        expect(res).to.be.an.object
      })
  })
})